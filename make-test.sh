RED='\033[1;31m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
NC='\033[0m' # No Color

if [ ! -d "build" ]; then
	mkdir build
fi

cd build

printf "${RED}Executando cmake...\n${NC}"
cmake -DETAPA_1=OFF -DETAPA_2=ON .. > ../output_cmake.log

printf "${YELLOW}Executando make...\n${NC}"
make > ../output_make.log

printf "${BLUE}Executando teste...\n${NC}"
ctest -R e2 > ../output_test.log

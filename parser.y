/*
    Grupo 02
  	Henrique Valcanaia
  	Jonas Bohrer
  	Renan Bortoluzzi
*/
%{
#include <stdio.h>
#include "main.h"
#include "cc_ast.h"
#include <stdlib.h>
#include <string.h>

comp_tree_t *tree;
comp_tree_t *function;
extern void* comp_tree_last;
extern struct comp_dict* hashTableSymbol;
%}
%union{
	struct comp_dict_item* valor_simbolo_lexico;
	//struct comp_tree* ast;
	struct comp_tree* val;
}


/* DeclaraÃ§Ã£o dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token<valor_simbolo_lexico> TK_OC_LE
%token<valor_simbolo_lexico> TK_OC_GE
%token<valor_simbolo_lexico> TK_OC_EQ
%token<valor_simbolo_lexico> TK_OC_NE
%token<valor_simbolo_lexico> TK_OC_AND
%token<valor_simbolo_lexico> TK_OC_OR
%token<valor_simbolo_lexico> TK_OC_SL
%token<valor_simbolo_lexico> TK_OC_SR
%token<valor_simbolo_lexico> TK_LIT_INT
%token<valor_simbolo_lexico> TK_LIT_FLOAT
%token<valor_simbolo_lexico> TK_LIT_FALSE
%token<valor_simbolo_lexico> TK_LIT_TRUE
%token<valor_simbolo_lexico> TK_LIT_CHAR
%token<valor_simbolo_lexico> TK_LIT_STRING
%token<valor_simbolo_lexico> TK_IDENTIFICADOR
%token TOKEN_ERRO

%type<val> inicio
%type<val> programa
%type<val> nome_funcao
%type<val> funcao_declara
%type<val> funcao_header
%type<val> funcao_corpo
%type<val> bloco_comando
%type<val> bloco_comando_corpo
%type<val> campo_novo
%type<val> comando_simples
%type<val> comando_simples_corpo
%type<val> atribuicao_vetor
%type<val> atribuicao_propriedades
%type<val> atribuicao_var
%type<val> dec_vetor_index
%type<val> comando_input
%type<val> comando_output
%type<val> chama_funcao
%type<val> argumentos_funcao
%type<val> comandos_fluxo
%type<val> comando_return
%type<val> comando_if
%type<val> comando_if_else
%type<val> comando_while
%type<val> comando_do
%type<val> expressao
%type<val> sub_expressao
%type<val> lista_expressao
%type<val> token_literal
%type<val> operador
%type<val> operando
%type<val> literais_numericos
%type<val> '+'
%type<val> '-'
%type<val> '/'
%type<val> '*'
%type<val> '='
%type<val> '<'
%type<val> '>'
%type<val> '%'
%type<val> '!'
%type<val> ';'

%%
/* Regras (e aÃ§Ãµes) da gramÃ¡tica */
inicio: programa {

		tree = tree_make_node(AST_PROGRAMA);	//cria nodo
		$$ = tree;				//associa o inicio Ã  raÃ­z da Ã¡rvore

		if ($1 != NULL) {
			tree_insert_node($$, $1);
			gv_declare(AST_PROGRAMA, $$, NULL);
		}
	};

programa: 	novo_tipo_declara  programa {$$ = $2;}|
		var_global_declara programa {$$ = $2;}|			
		funcao_declara programa {
			$$ = $1;
			//tree_insert_node($$, $1);

			if ($2 != NULL){
				comp_tree_t* node1 = $1;
				comp_tree_t* node2 = $2;
				node2->prev = node1;
				node2->prev->next = node2;
				gv_connect($$, $2);		
			}
			
		}
		| %empty {$$ = NULL;};

encapsulamento : TK_PR_PRIVATE | TK_PR_PUBLIC | TK_PR_PROTECTED;

tipo : TK_PR_INT | TK_PR_FLOAT | TK_PR_BOOL | TK_PR_CHAR | TK_PR_STRING;

is_static : TK_PR_STATIC | ;
token_literal : TK_LIT_INT | TK_LIT_FLOAT | TK_LIT_FALSE | TK_LIT_TRUE | TK_LIT_CHAR | TK_LIT_STRING;

/* Declaracao Novo Tipo - Item 2.1*/

campo_novo : encapsulamento tipo TK_IDENTIFICADOR;
campo_lista : campo_novo ':' campo_lista | campo_novo;
novo_tipo_declara : TK_PR_CLASS TK_IDENTIFICADOR '[' campo_lista ']' ';';

/* Variavel Global - Item 2.2 */

var_global_declara:	is_static tipo TK_IDENTIFICADOR ';' |
					is_static TK_IDENTIFICADOR TK_IDENTIFICADOR ';' |
					is_static tipo TK_IDENTIFICADOR '[' TK_LIT_INT ']' ';' |
					is_static TK_IDENTIFICADOR TK_IDENTIFICADOR '[' TK_LIT_INT ']' ';' ;
						
/* Funcao - Item 2.3 */
nome_funcao : TK_IDENTIFICADOR {
			$$ = tree_make_node($1);
			gv_declare(AST_FUNCAO, $$, $1->content->value.stringValue);
		};
funcao_declara : funcao_header funcao_corpo {
			$$ = $1;

			if ($2 != NULL) {
				tree_insert_node($$, $2);
			}
		};
funcao_parametro : TK_PR_CONST tipo TK_IDENTIFICADOR | tipo TK_IDENTIFICADOR;
funcao_parametro_lista : funcao_parametro_lista ',' funcao_parametro | funcao_parametro ;
funcao_header : is_static tipo nome_funcao '(' funcao_parametro_lista ')' {$$=$3;}
	| is_static tipo nome_funcao '(' ')' {$$=$3;};
funcao_corpo : '{' bloco_comando_corpo '}' {$$ = $2;} | '{' bloco_comando '}' {$$ = $2;} | '{' '}' {$$ = NULL;};

/* Bloco de comandos - Item 2.4 */
bloco_comando : '{' bloco_comando_corpo '}' {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);
		} | '{' bloco_comando_corpo '}' bloco_comando {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);

			if ($4 != NULL) {
				comp_tree_t* node = $4;
				node->prev = $$;
				node->prev->next = node;
				gv_connect($$, $4);
			}
		} | '{' bloco_comando_corpo '}' ';' {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);
		} | '{' bloco_comando_corpo '}' ';' bloco_comando {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);

			if ($5 != NULL) {
				comp_tree_t* node = $5;
				node->prev = $$;
				node->prev->next = node;
				gv_connect($$, $5);
			}
		} | '{' '}' bloco_comando {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			if ($3 != NULL) {
				comp_tree_t* node = $3;
				node->prev = $$;
				node->prev->next = node;
				gv_connect($$, $3);
				}
		} | '{' '}' {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
		} | '{' '}' ';' bloco_comando {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			if ($4 != NULL) {
				comp_tree_t* node = $4;
				node->prev = $$;
				node->prev->next = node;
				gv_connect($$, $4);
				}
		} | '{' '}' ';' {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
		} | '{' bloco_comando '}' {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);
		} | '{' bloco_comando '}' ';'{	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);
		} | '{' bloco_comando '}' bloco_comando{	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);

			if ($4 != NULL) {
				comp_tree_t* node = $4;
				node->prev = $$;
				node->prev->next = node;
				gv_connect($$, $4);
			}
		} | '{' bloco_comando '}' ';' bloco_comando {	
			$$ = tree_make_node(AST_BLOCO); 
			gv_declare(AST_BLOCO, $$, NULL);
			tree_insert_node($$, $2);

			if ($5 != NULL) {
				comp_tree_t* node = $5;
				node->prev = $$;
				node->prev->next = node;
				gv_connect($$, $5);
			}
		};
bloco_comando_corpo : comando_simples bloco_comando_corpo {
			$$ = $1;

			if ($1 == NULL) $$ = $2;
			if ($2 != NULL && $1 != NULL) {
				comp_tree_t* node1 = $1;
				comp_tree_t* node2 = $2;
				node2->prev = node1;
				node2->prev->next = node2;
				gv_connect($1, $2);
			}
		}| {$$ = NULL;};

/*Comando Simples - Item 2.5*/
comando_simples : comando_simples_corpo ';' {$$ = $1;}| comando_case | ';' {$$ = NULL;};
comando_simples_corpo: declara_var_local {$$ = NULL;}| atribuicao_var {$$ = $1;}| comando_input {$$ = $1;}| comando_output | chama_funcao {$$ = $1;}| comando_shift | comando_return | comando_break | comando_continue | comandos_fluxo {$$ = $1;}| bloco_comando {$$ = $1;} | ;
lista_comando_simples: lista_comando_simples ',' comando_simples | comando_simples ;

/* Declaracao de variavel local */					
inicia_var_local : TK_OC_LE TK_IDENTIFICADOR | TK_OC_LE token_literal | ;

var_primitiva: 	TK_PR_STATIC tipo TK_IDENTIFICADOR inicia_var_local |
				tipo TK_IDENTIFICADOR inicia_var_local |
				TK_PR_STATIC TK_PR_CONST tipo TK_IDENTIFICADOR inicia_var_local |
				TK_PR_CONST tipo TK_IDENTIFICADOR inicia_var_local;

var_usuario: 	TK_IDENTIFICADOR TK_IDENTIFICADOR inicia_var_local |
				TK_PR_STATIC TK_IDENTIFICADOR TK_IDENTIFICADOR inicia_var_local |
				TK_PR_STATIC TK_PR_CONST TK_IDENTIFICADOR TK_IDENTIFICADOR inicia_var_local |
				TK_PR_CONST TK_IDENTIFICADOR TK_IDENTIFICADOR inicia_var_local;

declara_var_local : var_primitiva | var_usuario;

/* Comando de atribuicao */
atribuicao_var : TK_IDENTIFICADOR '=' expressao {
			$$ = tree_make_node($2);
			if ($3 != NULL) tree_insert_node($$, $3);
			if ($1 != NULL) tree_insert_node($$, $1);
			gv_declare(AST_ATRIBUICAO, $$, NULL);
			gv_declare(AST_IDENTIFICADOR, $1, $1->key->lexeme);

		}| dec_vetor_index '=' expressao {
			$$ = tree_make_node($2);
			if ($3 != NULL) tree_insert_node($$, $3);
			if ($1 != NULL) tree_insert_node($$, $1);
			gv_declare(AST_ATRIBUICAO, $$, NULL);

		}| TK_IDENTIFICADOR '!' campo_novo '='expressao ;

dec_vetor_index : TK_IDENTIFICADOR '[' expressao ']' {
			$$ = tree_make_node(AST_VETOR_INDEXADO);
			if ($1 != NULL) tree_insert_node($$, $1);
			if ($3 != NULL) tree_insert_node($$, $3);
			gv_declare(AST_VETOR_INDEXADO, $$, NULL);
			gv_declare(AST_IDENTIFICADOR, $1, $1->key->lexeme);
		};

/* Comandos de entrada e saida */
comando_input : TK_PR_INPUT expressao {
			$$ = tree_make_node(AST_INPUT);
			gv_declare(AST_INPUT, $$, NULL);
			if ($2 != NULL) tree_insert_node($$, $2);
		};
comando_output : TK_PR_OUTPUT lista_expressao {
			$$ = tree_make_node(AST_OUTPUT);
			gv_declare(AST_OUTPUT, $$, NULL);
			if ($2 != NULL) tree_insert_node($$, $2);
		};

/* Chamada de funcao */
chama_funcao : nome_funcao '(' argumentos_funcao ')' {
		$$ = tree_make_node(AST_CHAMADA_DE_FUNCAO);
		gv_declare(AST_CHAMADA_DE_FUNCAO, $$, NULL);
		if ($1 != NULL) tree_insert_node($$, $1);
		if ($3 != NULL) tree_insert_node($$, $3);
	}| nome_funcao '(' ')' {
		$$ = tree_make_node(AST_CHAMADA_DE_FUNCAO);
		gv_declare(AST_CHAMADA_DE_FUNCAO, $$, NULL);
		if ($1 != NULL) tree_insert_node($$, $1);
	};
argumentos_funcao: expressao ',' argumentos_funcao {
		$$ = $1;
		if ($3 != NULL) {
			comp_tree_t* node = $3;
			node->prev = $$;
			node->prev->next = node;
			gv_connect($$, $3);
		}
	}| expressao;

/*Comandos de Shift */
comando_shift: TK_IDENTIFICADOR TK_OC_SL TK_LIT_INT | TK_IDENTIFICADOR TK_OC_SR TK_LIT_INT;

/* Comandos de Retorno, Break, Continue e Case */
comando_return: TK_PR_RETURN expressao {
		$$ = tree_make_node(AST_RETURN);
		gv_declare(AST_RETURN, $$, NULL);
		if ($2 != NULL) tree_insert_node($$, $2);	
	};
comando_break: TK_PR_BREAK;
comando_continue: TK_PR_CONTINUE;
comando_case: TK_PR_CASE TK_LIT_INT ':';

/* Comandos de controle de fluxo */
comando_if: TK_PR_IF '(' expressao ')' TK_PR_THEN comando_simples_corpo {
		$$ = tree_make_node(AST_IF_ELSE);
		if ($6 != NULL) tree_insert_node($$, $6);
		if ($3 != NULL) tree_insert_node($$, $3);
		gv_declare(AST_IF_ELSE, $$, NULL);
	};
comando_if_else: TK_PR_IF '(' expressao ')' TK_PR_THEN comando_simples_corpo TK_PR_ELSE comando_simples_corpo {
		$$ = tree_make_node(AST_IF_ELSE);
		if ($3 != NULL) tree_insert_node($$, $3);
		if ($6 != NULL) tree_insert_node($$, $6);
		if ($8 != NULL) tree_insert_node($$, $8);
		gv_declare(AST_IF_ELSE, $$, NULL);
	};;
comando_foreach: TK_PR_FOREACH '(' TK_IDENTIFICADOR ':' lista_expressao ')' comando_simples;
comando_for: TK_PR_FOR '(' lista_comando_simples ':' expressao ':' lista_comando_simples ')' comando_simples;
comando_while: TK_PR_WHILE '(' expressao ')' TK_PR_DO comando_simples_corpo {
		$$ = tree_make_node(AST_WHILE_DO);
		if ($3 != NULL) tree_insert_node($$, $3);
		if ($6 != NULL) tree_insert_node($$, $6);
		gv_declare(AST_WHILE_DO, $$, NULL);
	};
comando_do: TK_PR_DO comando_simples_corpo TK_PR_WHILE '(' expressao ')' {
		$$ = tree_make_node(AST_DO_WHILE);
		if ($5 != NULL) tree_insert_node($$, $5);
		if ($2 != NULL) tree_insert_node($$, $2);
		gv_declare(AST_DO_WHILE, $$, NULL);
	};
comando_switch: TK_PR_SWITCH '(' expressao ')' comando_simples;

comandos_fluxo: comando_if_else {$$ = $1;} | comando_if {$$ = $1;} | comando_foreach | comando_for | comando_while | comando_do | comando_switch; 

/*Pode ser literais numericos*/
literais_numericos : TK_LIT_INT | TK_LIT_FLOAT;
operando: TK_LIT_INT {
		$$ = tree_make_node($1);
		gv_declare(AST_LITERAL, $$, $1->key->lexeme);
	}| TK_IDENTIFICADOR {
		$$ = tree_make_node($1);
		gv_declare(AST_IDENTIFICADOR, $$, $1->key->lexeme);
	}| TK_LIT_FLOAT {
		$$ = tree_make_node($1);
		gv_declare(AST_LITERAL, $$, $1->key->lexeme);
	}| TK_LIT_STRING {
		$$ = tree_make_node($1);
		gv_declare(AST_LITERAL, $$, $1->key->lexeme);
	}| TK_LIT_CHAR {
		$$ = tree_make_node($1);
		gv_declare(AST_LITERAL, $$, $1->key->lexeme);
	}| TK_LIT_FALSE {
		$$ = tree_make_node($1);
		gv_declare(AST_LITERAL, $$, $1->key->lexeme);
	}| TK_LIT_TRUE {
		$$ = tree_make_node($1);
		gv_declare(AST_LITERAL, $$, $1->key->lexeme);
	}| dec_vetor_index {
		$$ = $1;
	}| chama_funcao 
	| '(' expressao ')' {
		$$ = $2;
	}| '-' expressao {
		$$ = tree_make_node($1);
		gv_declare(AST_ARIM_INVERSAO, $$, NULL);
		
		if ($2!= NULL) {
			tree_insert_node($$, $2);
		}	
	}| '!' expressao {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_NEGACAO, $$, NULL);

		if ($2!= NULL) {
			tree_insert_node($$, $2);
		}	
	};

/* Operadores especificados */
operador : TK_OC_LE {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_LE, $$, NULL);
	}| TK_OC_GE {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_GE, $$, NULL);
	}| TK_OC_EQ {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_IGUAL, $$, NULL);
	}| TK_OC_NE {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_DIF, $$, NULL);
	}| TK_OC_AND {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_E, $$, NULL);
	}| TK_OC_OR {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_OU, $$, NULL);
	}| TK_OC_SL {
		$$ = tree_make_node($1);
		gv_declare(AST_SHIFT_LEFT, $$, NULL);
	}| TK_OC_SR {
		$$ = tree_make_node($1);
		gv_declare(AST_SHIFT_RIGHT, $$, NULL);
	}| '*' {
		$$ = tree_make_node($1);
		gv_declare(AST_ARIM_MULTIPLICACAO, $$, NULL);
	}| '+' {
		$$ = tree_make_node($1);
		gv_declare(AST_ARIM_SOMA, $$, NULL);
	}| '/' {
		$$ = tree_make_node($1);
		gv_declare(AST_ARIM_DIVISAO, $$, NULL);
	}| '-' {
		$$ = tree_make_node($1);
		gv_declare(AST_ARIM_SUBTRACAO, $$, NULL);
	}| '%' 
	| '<' {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_L, $$, NULL);
	}| '>' {
		$$ = tree_make_node($1);
		gv_declare(AST_LOGICO_COMP_G, $$, NULL);
	};

/*Expressoes - Item 2.6*/
expressao: operando operador expressao {
		$$ = $2;
		if ($3!= NULL) tree_insert_node($$, $3);
		if ($1!= NULL) tree_insert_node($$, $1);
	}| operando {$$ = $1;}; //Recursao nas operacoes
lista_expressao: expressao {$$= $1;}| expressao ',' lista_expressao {$$ = $1; if ($3!= NULL) tree_insert_node($$, $3);};

%%
